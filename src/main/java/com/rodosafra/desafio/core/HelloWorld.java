package com.rodosafra.desafio.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HelloWorld {
    @JsonProperty
    private String text;

    public HelloWorld() {}

    public HelloWorld(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
