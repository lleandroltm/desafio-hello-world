package com.rodosafra.desafio.resources;

import com.rodosafra.desafio.core.HelloWorld;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/Hello")
@Produces(MediaType.APPLICATION_JSON)
public class HelloWorldResources {

    @GET
    public Response getText(){
        return Response.ok(new HelloWorld("HelloWord")).build();
    }
}
