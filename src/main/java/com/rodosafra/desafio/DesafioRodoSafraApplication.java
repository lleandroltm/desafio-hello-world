package com.rodosafra.desafio;

import com.rodosafra.desafio.resources.HelloWorldResources;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class DesafioRodoSafraApplication extends Application<DesafioRodoSafraConfiguration> {

    public static void main(final String[] args) throws Exception {
        new DesafioRodoSafraApplication().run(args);
    }

    @Override
    public String getName() {
        return "DesafioRodoSafra";
    }

    @Override
    public void initialize(final Bootstrap<DesafioRodoSafraConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final DesafioRodoSafraConfiguration configuration,
                    final Environment environment) {
        HelloWorldResources helloWorldResources = new HelloWorldResources();
        environment.jersey().register(helloWorldResources);
    }

}
